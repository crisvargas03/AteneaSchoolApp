﻿using System.Net;

namespace AteneaBackend.Datalayer.Utils
{
    public class APIResponse
    {
        public APIResponse()
        {
            ErrorMessages = new List<string>();
            IsSuccess = true;
            StatusCode = HttpStatusCode.OK;
        }
        public HttpStatusCode StatusCode { get; set; }
        public bool IsSuccess { get; set; } = true;
        public List<string>? ErrorMessages { get; set; }
        public object? Payload { get; set; }


        public APIResponse FailedResponse(HttpStatusCode statusCode, string error)
        {
            StatusCode = statusCode;
            Payload = null;
            IsSuccess = false;
            ErrorMessages!.Add(error);

            return this;
        }
    }
}
