﻿using System.Security.Cryptography;
using System.Text;

namespace AteneaBackend.Businesslayer.Utils
{
    public static class StringEncrypter
    {
        public static string Encrypt(this string input)
        {
            using (SHA256 sha256 = SHA256.Create())
            {
                byte[] hashBytes = sha256.ComputeHash(Encoding.UTF8.GetBytes(input));
                StringBuilder builder = new StringBuilder();
                for (int i = 0; i < hashBytes.Length; i++)
                {
                    builder.Append(hashBytes[i].ToString("x2"));
                }
                return builder.ToString();
            }
        }

        public static bool Decode(this string input, string hashedValue)
        {
            string hashedInput = Encrypt(input);
            return hashedInput.Equals(hashedValue, StringComparison.OrdinalIgnoreCase);
        }
    }
}
