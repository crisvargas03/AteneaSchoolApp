﻿using System.Formats.Asn1;
using System.Net;
using AteneaBackend.Businesslayer.Interfaces;
using AteneaBackend.Businesslayer.ViewModels.Attendance;
using AteneaBackend.Datalayer.Context;
using AteneaBackend.Datalayer.Models;
using AteneaBackend.Datalayer.Utils;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;

namespace AteneaBackend.Businesslayer.Services
{
    public class AttendanceService : IAttendanceService
    {
        private readonly MainBDContext _mainContext;
        private readonly IMapper _mapper;
        protected APIResponse _apiResponse;

        public AttendanceService(MainBDContext mainContext, IMapper mapper)
        {
            _mainContext = mainContext;
            _mapper = mapper;
            _apiResponse = new APIResponse();
        }

        public async Task<IEnumerable<AttendanceViewModel>> GetAll()
        {
            var attendances = _mainContext.Attendance.ProjectTo<AttendanceViewModel>(_mapper.ConfigurationProvider);
            return await attendances.ToListAsync();
        }

        public async Task<List<int>> GetTodayId(DateTime date)
        {
            var ids = await _mainContext.Attendance.Where(x => x.CreationDate.Date == date.Date).Select(x => x.Id).ToListAsync();
            return ids;
        } 

        public async Task<APIResponse> Save(AttendanceInputModel inputModel)
        {
            if (inputModel is null)
                return _apiResponse.FailedResponse(HttpStatusCode.BadRequest, "Invalid Information");

            if (! await CheckStudent(inputModel.StudentId)) 
                return _apiResponse.FailedResponse(HttpStatusCode.BadRequest, "Student was not found");

            var mappedInput = _mapper.Map<Attendance>(inputModel);
            _mainContext.Attendance.Add(mappedInput);
            var result = await _mainContext.SaveChangesAsync();
            if (result >= 1)
            {
                var newAttendance = await GetLast();
                _apiResponse.Payload = _mapper.Map<AttendanceViewModel>(newAttendance);
                return _apiResponse;
            }

            return null;
        }

        private async Task<Attendance> GetLast() => await _mainContext.Attendance.OrderByDescending(s => s.Id).FirstOrDefaultAsync();

        protected async Task<bool> CheckStudent(int studentId) => await _mainContext.Student.AnyAsync(s => s.Id == studentId);

        public async Task<bool> CheckAttendance(int student) => await _mainContext.Attendance
            .AnyAsync(at => at.StudentId == student && at.CreationDate.Date == DateTime.Now.Date);
    }
}
