﻿using AteneaBackend.Businesslayer.Interfaces;
using AteneaBackend.Businesslayer.Utils;
using AteneaBackend.Businesslayer.ViewModels.Teacher;
using AteneaBackend.Datalayer.Context;
using AteneaBackend.Datalayer.Models;
using AteneaBackend.Datalayer.Utils;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using System.Net;

namespace AteneaBackend.Businesslayer.Services
{
    public class AuthService : IAuthService
    {
        private readonly MainBDContext _mainContext;
        private readonly IMapper _mapper;
        private readonly ITokenService _tokenService;
        protected APIResponse ApiResponse;

        public AuthService(MainBDContext mainContext, IMapper mapper, ITokenService tokenService)
        {
            _mainContext = mainContext;
            _mapper = mapper;
            _tokenService = tokenService;
            ApiResponse = new();
        }

        public async Task<APIResponse> LoginWithCreds(TeacherLoginInputModel creds)
        {
            var teacherInfo = await _mainContext.Teacher
                .Where(t => t.Email == creds.Email)
                .FirstOrDefaultAsync();

            var isCorrect = (creds?.Password).Decode(teacherInfo?.Password);

            if (teacherInfo != null && isCorrect)
            {
                var token = _tokenService.Generate(teacherInfo.Email);
                ApiResponse.Payload = token;
                return ApiResponse;
            }

            return ApiResponse.FailedResponse(HttpStatusCode.BadRequest, "can not login, please check your information");
        }

        public async Task<APIResponse> Register(TeacherRegisterInputModel registerModel)
        {
            if (registerModel is null) 
                return ApiResponse.FailedResponse(HttpStatusCode.BadRequest, "can not register the user...");

            if (!registerModel.Email.Contains('@'))
                return ApiResponse.FailedResponse(HttpStatusCode.BadRequest, "Invalid email address");

            if (registerModel.Password.Length < 8)
                return ApiResponse.FailedResponse(HttpStatusCode.BadRequest, "Too short password");

            if (await _mainContext.Teacher.AnyAsync(t => t.Email == registerModel.Email)) 
                return ApiResponse.FailedResponse(HttpStatusCode.BadRequest, "can not register the user...");

            var mappedInput = _mapper.Map<Teacher>(registerModel);
            mappedInput.Password = mappedInput.Password.Encrypt();

            _mainContext.Teacher.Add(mappedInput);
            var result = await _mainContext.SaveChangesAsync();

            if (result >= 1)
            {
                var newTeacher = await GetLast();
                var token = _tokenService.Generate(newTeacher.Email);
                ApiResponse.Payload = token;
                return ApiResponse;
            }
            return ApiResponse.FailedResponse(HttpStatusCode.BadRequest, "can not register the user...");
        }

        private async Task<Teacher> GetLast() => await _mainContext.Teacher.OrderByDescending(s => s.Id).FirstOrDefaultAsync();
    }
}
