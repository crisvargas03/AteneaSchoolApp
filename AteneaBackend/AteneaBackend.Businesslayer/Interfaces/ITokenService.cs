﻿namespace AteneaBackend.Businesslayer.Interfaces
{
    public interface ITokenService
    {
        string Generate(string email);
    }
}
