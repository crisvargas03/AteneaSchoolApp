﻿using AteneaBackend.Businesslayer.ViewModels.Teacher;
using AteneaBackend.Datalayer.Utils;

namespace AteneaBackend.Businesslayer.Interfaces
{
    public interface IAuthService
    {
        Task<APIResponse> LoginWithCreds(TeacherLoginInputModel creds);
        Task<APIResponse> Register(TeacherRegisterInputModel registerModel);
    }
}
