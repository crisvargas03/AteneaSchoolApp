﻿using AteneaBackend.Businesslayer.ViewModels.Attendance;
using AteneaBackend.Datalayer.Utils;

namespace AteneaBackend.Businesslayer.Interfaces
{
    public interface IAttendanceService
    {
        Task<bool> CheckAttendance(int student);
        Task<IEnumerable<AttendanceViewModel>> GetAll();
        Task<List<int>> GetTodayId(DateTime date);
        Task<APIResponse> Save(AttendanceInputModel inputModel);
    }
}
