using AteneaBackend.Configuration;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using System.Text;
using AspNetCoreRateLimit;

var builder = WebApplication.CreateBuilder(args);

// Add DB Context configuration
builder.Services.ConfigureDBContext(builder.Configuration);

// Add the dependency injection configuration
builder.Services.ConfigureInjections();

// Add AutoMapper Configuration
builder.Services.ConfigureAutoMapper();

builder.Services.AddControllers();
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen(op =>
{
    op.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
    {
        Description = "JWT Authorization header using the Bearer scheme. \r\n\r\n " +
            "Enter 'Bearer' [space] and then your token in the text input below.\r\n\r\n" +
            "Example: \"Bearer 12345abcdef\"",
        Name = "Authorization",
        In = ParameterLocation.Header,
        Scheme = "Bearer"
    });
    op.AddSecurityRequirement(new OpenApiSecurityRequirement()
    {
        {
            new OpenApiSecurityScheme
            {
                Reference = new OpenApiReference
                {
                    Type = ReferenceType.SecurityScheme,
                    Id = "Bearer"
                },
                Scheme = "oauth2",
                Name = "Bearer",
                In = ParameterLocation.Header
            },
            new List<string>()
        }
    });
});

var key = builder.Configuration.GetValue<string>("ApiSettings:SECRET-KEY");
builder.Services.AddAuthentication(a =>
{
    a.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
    a.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
}).AddJwtBearer(t =>
{
    t.RequireHttpsMetadata = false;
    t.SaveToken = true;
    t.TokenValidationParameters = new TokenValidationParameters
    {
        ValidateIssuerSigningKey = true,
        IssuerSigningKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(key)),
        ValidateIssuer = false,
        ValidateAudience = false,
    };
});

//services cors
var origins = builder.Configuration.GetValue<string>("AllowedHosts").Split(",");
builder.Services.AddCors(p => p.AddPolicy("CORS", builder =>
{
    builder.WithOrigins(origins)
    .AllowAnyMethod()
    .AllowAnyHeader();
}));

// Add Rate limit
builder.Services.AddMemoryCache();
builder.Services.Configure<IpRateLimitOptions>(op =>
{
    op.EnableEndpointRateLimiting = true;
    op.StackBlockedRequests = false;
    op.HttpStatusCode = 429;
    op.RealIpHeader = "";
    op.ClientIdHeader = "";
    op.GeneralRules = new List<RateLimitRule>
    {
        new RateLimitRule
        {
            Endpoint = "*",
            Period = "20s",
            Limit = 3
        }
    };
});

var app = builder.Build();

app.UseSwagger();
app.UseSwaggerUI(op =>
{
    op.SwaggerEndpoint("/swagger/v1/swagger.json", "Atenea-API");
});

app.UseCors("CORS");
app.UseHttpsRedirection();

app.UseAuthentication();
app.UseAuthorization();

app.MapControllers();

app.Run();
