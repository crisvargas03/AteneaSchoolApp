﻿using AteneaBackend.Businesslayer.Interfaces;
using AteneaBackend.Businesslayer.ViewModels.Attendance;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace AteneaBackend.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class AttendanceController : ControllerBase
    {
        private readonly IAttendanceService _attendanceService;

        public AttendanceController(IAttendanceService attendanceService)
        {
            _attendanceService = attendanceService;
        }

        [HttpGet]
        public async Task<IActionResult> Get()
        {
            return Ok(await _attendanceService.GetAll());
        }

        [HttpGet("today")]
        public async Task<IActionResult> GetStudentId()
        {
            return Ok(await _attendanceService.GetTodayId(DateTime.Now));
        }

        [HttpPost]
        public async Task<IActionResult> Save([FromBody] AttendanceInputModel attendanceInputModel)
        {

            var todayPresent = await _attendanceService.CheckAttendance(attendanceInputModel.StudentId);
            if (!todayPresent)
            {
                var registerResult = await _attendanceService.Save(attendanceInputModel);
                if (registerResult.IsSuccess)
                    return Ok(registerResult);

                return BadRequest(registerResult);
            }
            return BadRequest("Already created...");
        }
    }
}
